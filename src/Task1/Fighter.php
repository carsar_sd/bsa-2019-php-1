<?php declare(strict_types=1);

namespace App\Task1;

class Fighter
{
    /**
     * @var int
     */
    public $fID;
    /**
     * @var string
     */
    public $fName;
    /**
     * @var int
     */
    public $fHealth;
    /**
     * @var int
     */
    public $fAttack;
    /**
     * @var string
     */
    public $fImage;


    /**
     * Initialize fighter
     *
     * @param int $fID
     * @param string $fName
     * @param int $fHealth
     * @param int $fAttack
     * @param string $fImage
     * @return void
     */
    public function __construct(int $fID, string $fName, int $fHealth, int $fAttack, string $fImage)
    {
        $this->fID     = $fID;
        $this->fName   = $fName;
        $this->fHealth = $fHealth;
        $this->fAttack = $fAttack;
        $this->fImage  = $fImage;
    }


    /**
     * Get fighter id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->fID;
    }


    /**
     * Get fighter name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->fName;
    }


    /**
     * Get fighter health
     *
     * @return int
     */
    public function getHealth(): int
    {
        return $this->fHealth;
    }


    /**
     * Get fighter attack
     *
     * @return int
     */
    public function getAttack(): int
    {
        return $this->fAttack;
    }


    /**
     * Get fighter image
     *
     * @return string
     */
    public function getImage(): string
    {
        return $this->fImage;
    }
}
