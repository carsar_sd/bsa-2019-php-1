<?php declare(strict_types=1);

namespace App\Task1;

use App\Task1\Fighter;

class FightArena
{
    /**
     * @var array
     */
    public $arena = [];


    /**
     * Show all fighters in arena
     *
     * @return array
     */
    public function all(): array
    {
        return $this->arena;
    }


    /**
     * Add new fighter to arena
     *
     * @param Fighter $fighter
     * @return void
     */
    public function add(Fighter $fighter): void
    {
        $this->arena[] = $fighter;
    }


    /**
     * Get mostPowerful fighter in arena
     *
     * @return Fighter|null
     */
    public function mostPowerful(): ?Fighter
    {
        $fighter = null;

        if (count($this->arena) > 1) {
            /** -- sort via attack **/
            $arena = $this->arena;
            usort($arena, function($first, $second) {
                return ($first->fAttack < $second->fAttack && $first->fHealth > $second->fHealth);
            });
            $fighter = current($arena);
            /** // sort via attack **/
        }

        return $fighter;
    }


    /**
     * Get mostHealthy fighter in arena
     *
     * @return Fighter|null
     */
    public function mostHealthy(): ?Fighter
    {
        $fighter = null;

        if (count($this->arena) > 1) {
            /** -- sort via health **/
            $arena = $this->arena;
            usort($arena, function($first, $second) {
                return ($first->fHealth < $second->fHealth && $first->fAttack > $second->fAttack);
            });
            $fighter = current($arena);
            /** // sort via health **/
        }

        return $fighter;
    }
}
